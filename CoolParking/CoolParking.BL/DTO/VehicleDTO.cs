﻿using System;
using System.Collections.Generic;
using System.Text;
using CoolParking.BL.Models;
using Newtonsoft.Json;

namespace CoolParking.BL.DTO
{
    public class VehicleDTO
    {
        public string id;
        public string Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }
        public decimal Sum { get; set; }

        public VehicleDTO()
        {

        }

        public VehicleDTO(string id,  decimal sum)
        {
            this.id = id;
            Sum = sum;
        }

        public VehicleDTO(string id, VehicleType vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
    }
}

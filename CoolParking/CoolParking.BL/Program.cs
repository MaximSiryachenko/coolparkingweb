﻿using CoolParking.BL.Services;
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Text;
using System.Text.RegularExpressions;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;

namespace CoolParking.BL
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hello! It's CoolParking service.");
            Menu();

            Console.ReadKey();
        }

        static void Menu()
        {
            ParkingService service = new ParkingService();
            Console.WriteLine("Enter the number of the required option [1-9]:");
            string choose = "1";
            Console.WriteLine("1 - Display the current Parking balance");
            Console.WriteLine("2 - Display the amount of money earned for the current period");
            Console.WriteLine("3 - Display the number of available / occupied parking spaces");
            Console.WriteLine("4 - Display all Parking Transactions for the current period");
            Console.WriteLine("5 - Display the transaction history (Transactions.log)");
            Console.WriteLine("6 - Display the list of Vehicles in the Parking lot");
            Console.WriteLine("7 - Add the Vehicle on the Parking");
            Console.WriteLine("8 - Pick up a vehicle from Parking");
            Console.WriteLine("9 - Top up the balance of a specific Vehicle");
            choose = Console.ReadLine();
            if (!Regex.IsMatch(choose, "[1-9]{1}"))
            {
                Console.Clear();
                Menu();
            }
            string id;
            TransactionInfo[] lastParkingTransactions;
            switch (choose)
            {
                case "1":
                    Console.Write("\nCurrent Parking balance: ");
                    Console.WriteLine(service.GetBalance());
                    break;
                case "2":
                    Console.Write("\nAmount of money earned for the current period: ");
                    lastParkingTransactions = service.GetLastParkingTransactions();
                    Console.WriteLine(lastParkingTransactions.Sum(tr => tr.Sum));
                    break;
                case "3":
                    Console.Write("\nNumber of available / occupied parking spaces: ");
                    Console.WriteLine(service.GetFreePlaces() + " / " + (service.GetCapacity() - service.GetFreePlaces()));
                    break;
                case "4":
                    Console.WriteLine("\nAll Parking Transactions for the current period:");
                    lastParkingTransactions = service.GetLastParkingTransactions();
                    foreach (TransactionInfo t in lastParkingTransactions)
                    {
                        Console.WriteLine("Time: " + t.Time + "\t ID: " + t.Id + "\t Balance: " + t.Sum);
                    }
                    break;
                case "5":
                    Console.WriteLine("\nTransaction history (Transactions.log):");
                    Console.Write(service.ReadFromLog());
                    break;
                case "6":
                    Console.WriteLine("\nList of Vehicles in the Parking lot");
                    ReadOnlyCollection<Vehicle> vehicles = service.GetVehicles();
                    foreach (Vehicle v in vehicles)
                    {
                        Console.WriteLine("ID: " + v.Id + "\t Type: " + v.VehicleType + "\t Balance: " + v.Balance);
                    }
                    break;
                case "7":
                    Console.WriteLine("\nAdding the Vehicle on the Parking");
                    Console.Write("Enter ID in format AA-0000-AA: ");
                    id = Console.ReadLine();
                    if (!service.CheckNumber(id))
                    {
                        Console.WriteLine("Invalid ID Format! Press any key...");
                        Console.ReadKey();
                        Console.Clear();
                        Menu();
                    }

                    Console.WriteLine("Type: ");
                    Console.WriteLine("\t 1) Passenger Car");
                    Console.WriteLine("\t 2) Truck");
                    Console.WriteLine("\t 3) Bus");
                    Console.WriteLine("\t 4) Motorcycle");
                    Console.Write("Enter number of type a car: ");
                    string typeNumber = Console.ReadLine();
                    if (!Regex.IsMatch(typeNumber, "[1-4]{1}"))
                    {
                        Console.WriteLine("Invalid Type Format! Press any key...");
                        Console.ReadKey();
                        Console.Clear();
                        Menu();
                    }
                    int typeN = Convert.ToInt32(typeNumber);
                    typeN--;
                    VehicleType type = (VehicleType)Convert.ToInt32(typeN);

                    Console.WriteLine("Vehicle added with 0 balance! Top Up balance!");
                    Vehicle veh = new Vehicle(id, type, 0);
                    service.AddVehicle(veh);
                    Console.WriteLine(Parking.Vehicles[0].Id + " " + Parking.Vehicles[0].VehicleType + " " + Parking.Vehicles[0].Balance);
                    Console.ReadKey();
                    break;
                case "8":
                    Console.WriteLine("\nPick up a vehicle from Parking");
                    Console.Write("Enter vehicle number (id) in format AA-0000-AA: ");
                    id = Console.ReadLine();
                    if (!service.CheckNumber(id))
                    {
                        Console.WriteLine("Invalid ID Format! Press any key...");
                        Console.ReadKey();
                        Console.Clear();
                        Menu();
                    }
                    service.RemoveVehicle(id);
                    break;
                case "9":
                    Console.WriteLine("\nTop up the balance of Vehicle");
                    Console.WriteLine("Enter vehicle number (id) in format AA-0000-AA: ");
                    id = Console.ReadLine();
                    if (!service.CheckNumber(id))
                    {
                        Console.WriteLine("Invalid ID Format! Press any key...");
                        Console.ReadKey();
                        Console.Clear();
                        Menu();
                    }
                    Console.Write("Enter recharge amount: ");
                    string str = Console.ReadLine();
                    decimal sum = 0;
                    if (Decimal.TryParse(str, out sum))
                    {
                        service.TopUpVehicle(id, sum);
                    }
                    else
                    {
                        Console.WriteLine("Invalid amount Format! Press any key...");
                        Console.ReadKey();
                        Console.Clear();
                        Menu();
                    }
                    break;

            }
            Menu();
        }
    }
}


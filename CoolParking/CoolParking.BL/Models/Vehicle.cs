﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the Identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a valIdation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique Identifier.


using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [JsonProperty(PropertyName = "Id")]
        public readonly string Id;
        [JsonProperty(PropertyName = "VehicleType")]
        public readonly VehicleType VehicleType;
        public decimal Balance { get; set; }

        public Vehicle()
        {
            this.Id = GenerateRandomRegistrationPlateNumber();
            this.VehicleType = VehicleType.PassengerCar;
            this.Balance = 0;

        }

        public Vehicle(string Id, VehicleType vehicleType, decimal Balance)
        {
            if (!Regex.IsMatch(Id,"[A-Z]{2}-[0-9]{4}-[A-Z]{2}") || Balance < 0)
                throw new ArgumentException();
            this.Id = Id;
            this.VehicleType = vehicleType;
            this.Balance = Balance;
        }




        public static string  GenerateRandomRegistrationPlateNumber()
        {
            string s = "", symb = "ABCDEFGHIJKLMNOPQRSTUVWXYZ", nums = "0123456789";
            Random rnd = new Random();
            for (int i = 0; i < 2; i++)
                s += symb[rnd.Next(0, symb.Length)];
            s += "-";
            for (int i = 0; i < 4; i++)
                s += nums[rnd.Next(0, nums.Length)];
            s += "-";
            for (int i = 0; i < 2; i++)
                s += symb[rnd.Next(0, symb.Length)];
            return s;
        }
    }
}

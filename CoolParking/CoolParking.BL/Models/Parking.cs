﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.



using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public static decimal balance;
        public static List<Vehicle> Vehicles;
        public static List<TransactionInfo> Transactions;
        static Parking() {
            Vehicles = new List<Vehicle>();
            Transactions = new List<TransactionInfo>();
            balance = 0;
        }

        public Parking() { }

        public static readonly Parking p = new Parking();
        public static Parking P
        {
            get
            {
                return p;
            }
        }

    }
}

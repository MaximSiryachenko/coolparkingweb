﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.


using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        [JsonProperty(PropertyName = "vehicleId")]
        public string Id;
        [JsonProperty(PropertyName = "sum")]
        public decimal sum;
        [JsonIgnore]
        public decimal Sum
        {
            get
            {
                return sum;
            }

            set
            {
                sum = value;
            }
        }
        [JsonProperty(PropertyName = "transactionDate")]
        public DateTime Time;
    }


}

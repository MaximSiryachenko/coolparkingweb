﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    internal static class Settings
    {
        internal static int Capacity = 10;
        internal static int BillingPeriod = 5000;
        internal static int LoggingPeriod = 60000;
        internal static decimal Fine = 2.5m;
        internal static decimal[] tariff = new decimal[] {2, 5, 3.5m, 1 };
    }
}

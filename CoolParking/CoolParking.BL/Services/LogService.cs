﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

// TODO: реализовать класс LogService из интерфейса ILogService.
//       Одно явное требование - для метода read, если файл не найден, должно быть выброшено InvalidOperationException
//       Другие детали реализации зависят от вас, они просто должны соответствовать требованиям интерфейса
//       и тесты, например, в LogServiceTests вы можете найти необходимый формат конструктора.

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService: ILogService
    {
        public string LogPath { get; } = @"./Transactions.log";


        public LogService()
        {
            StreamWriter LogFile = new StreamWriter(LogPath, true);
            LogFile.Close();
        }

        public LogService(string path)
        {
            StreamWriter LogFile = new StreamWriter(path, true);
            LogFile.Close();
        }

        public void Write(string logInfo)
        {
            try
            {
                StreamWriter LogFile = new StreamWriter(LogPath, true);
                LogFile.Write(logInfo);
                LogFile.Write("\n");
                LogFile.Close();

        }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка создания/записи лога!");
                Console.WriteLine("Exception: " + e.Message);
            }
}

        public string Read()
        {
            string logs = "";
            string line;
            try
            {
                StreamReader LogFile = new StreamReader(LogPath);
                while ((line = LogFile.ReadLine()) != null)
                {
                    logs += line + "\n";
                }
                LogFile.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка чтения файла лога!");
                Console.WriteLine("Exception: " + e.Message);
            }
            return logs;
        }
    }
}

﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

// TODO: реализовать класс ParkingService из интерфейса IParkingService.
//       Для попытки добавить автомобиль на полной парковке должно быть сгенерировано исключение InvalidOperationException.
//       Для попытки удалить транспортное средство с отрицательным балансом (долгом) должно быть выброшено InvalidOperationException.
//       Другие правила проверки и формат конструктора взяты из тестов.
//       Другие детали реализации зависят от вас, они просто должны соответствовать требованиям интерфейса
//       и тесты, например, в ParkingServiceTests вы можете найти необходимый формат конструктора и правила проверки.

using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Timers;
using System.Transactions;
using System.Runtime.Serialization.Formatters;
using System.Runtime.ConstrainedExecution;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Globalization;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService, IDisposable
    {
        ITimerService billingTimer, logTimer;
        ILogService logService;

        public ParkingService()
        {
            Parking p = new Parking();
            logService = new LogService();
            billingTimer = new TimerService();
            logTimer = new TimerService();

            billingTimer.Interval = Settings.BillingPeriod;
            logTimer.Interval = Settings.LoggingPeriod;

            billingTimer.Elapsed += Billing;
            billingTimer.Start();

            logTimer.Elapsed += WriteToLog;
            logTimer.Start();
        }


        public ParkingService(ITimerService billingTimer, ITimerService logTimer, ILogService logService)
        {
            Parking p = new Parking();
            this.logService = logService;
            this.billingTimer = billingTimer;
            this.logTimer = logTimer;
            this.billingTimer.Interval = Settings.BillingPeriod;
            this.logTimer.Interval = Settings.LoggingPeriod;
            this.billingTimer.Elapsed += Billing;
            this.billingTimer.Start();
            this.logTimer.Elapsed += WriteToLog;
            this.logTimer.Start();
        }


        public decimal GetBalance()
        {
            return Parking.balance;
        }

        public int GetCapacity()
        {
            return Settings.Capacity;
        }

       public int GetFreePlaces()
       {
           return Settings.Capacity - Parking.Vehicles.Count;
       }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection <Vehicle> v =  new ReadOnlyCollection<Vehicle> (Parking.Vehicles);
            return v;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (CheckVehicle(vehicle))
                throw new ArgumentException();
            Parking.Vehicles.Add(vehicle);

        }

        public void RemoveVehicle(string vehicleId)
        {
            for (int i = 0; i < Parking.Vehicles.Count; i++)
            {
                if (Parking.Vehicles[i].Id == vehicleId)
                {
                    Parking.Vehicles.RemoveAt(i);
                    return;
                }
            }
            throw new ArgumentException();

        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            Vehicle v = Parking.Vehicles.Find((item) => item.Id == vehicleId);
            if (sum <= 0 || v == null)
                throw new ArgumentException();
            v.Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Parking.Transactions.ToArray();
        }

        public void Billing(object sender, ElapsedEventArgs e)
        { 
            if (Parking.Vehicles.Count > 0)
            {
                for (int i = 0; i < Parking.Vehicles.Count; i++)
                {
                    TransactionInfo t = new TransactionInfo();
                    decimal sum;
                    int tarif = (int)Parking.Vehicles[i].VehicleType;
                    if (Parking.Vehicles[i].Balance >= Settings.tariff[tarif])
                    {
                        sum = Settings.tariff[tarif];
                    }
                    else if (Parking.Vehicles[i].Balance <= 0)
                    {
                        sum = Settings.tariff[tarif] * Settings.Fine;
                    }
                    else
                    {
                        sum = ((Settings.tariff[tarif] - Parking.Vehicles[i].Balance) * Settings.Fine) + Parking.Vehicles[i].Balance;
                    }
                    Parking.Vehicles[i].Balance -= sum;
                    Parking.balance += sum;
                    t.Id = Parking.Vehicles[i].Id;
                    t.Time = DateTime.Now;
                    t.Sum = sum;
                    Parking.Transactions.Add(t);
                }
            }
        }


        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void WriteToLog(object sender, ElapsedEventArgs e)
        {
            for (int i = 0; i < Parking.Transactions.Count; i++)
            {
                logService.Write(Parking.Transactions[i].Time.ToString("d/MM/yyyy hh:mm:ss tt: ", CultureInfo.InvariantCulture) + 
                                 Convert.ToString(Math.Truncate(Parking.Transactions[i].Sum)) + "." + Math.Round((Parking.Transactions[i].Sum - Math.Truncate(Parking.Transactions[i].Sum)) * 100) +
                                 " money withdrawn from vehicle with Id='" + 
                                 Convert.ToString(Parking.Transactions[i].Id) + 
                                 "'.");
            }
            Parking.Transactions.Clear();

        }

        public void Dispose()
        {
            Parking.Vehicles.Clear();
            Parking.Transactions.Clear();
            Parking.balance = 0;
        }

        public bool CheckNumber(string id)
        {
            if (Regex.IsMatch(id, "[A-Z]{2}-[0-9]{4}-[A-Z]{2}"))
            {
                return true;
            }
            else
                return false;
        }

        public bool CheckVehicle(Vehicle vehicle)
        {
            Vehicle v = Parking.Vehicles.Find((item) => item.Id == vehicle.Id);
            if (v != null)
                return true;
            return false;
        }
    }
}

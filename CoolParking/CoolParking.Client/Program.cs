﻿using Microsoft.CodeAnalysis.FlowAnalysis;
using System;
using System.Net.Http;
using System.Text.RegularExpressions;

namespace CoolParking.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu menu = new Menu();
            ActionRequest(menu);
            //Console.WriteLine("Press Enter!");
            //Console.ReadKey();
            //Console.WriteLine(menu.DisplayBalance());
            //Console.WriteLine(menu.DisplayCapacity());
            //Console.WriteLine(menu.DisplayFreePlaces());
            //Console.WriteLine(menu.DisplayVehicles());
            //Console.WriteLine(menu.DisplayVehicle("BB-1111-BB"));
            //menu.AddVehicle("AA-0000-AA", 2, 300);
            //Console.WriteLine("Press Enter!");
            //Console.ReadKey();
            ////menu.DeleteVehicle("BB-1111-BB");
            //Console.WriteLine(menu.GetLastTransaction());
            //Console.WriteLine(menu.GetTransactions());
            //menu.TopUpVehicle("AA-0000-AA", 10000);
            //Console.ReadKey();

        }

        public static void ActionRequest(Menu menu)
        {
            Console.WriteLine("Enter the number of the required option [1-9]:");
            string choose = "1";
            Console.WriteLine("1 - Display the current Parking balance");
            Console.WriteLine("2 - Display the amount of money earned for the current period");
            Console.WriteLine("3 - Display the number of available / occupied parking spaces");
            Console.WriteLine("4 - Display the transaction history (Transactions.log)");
            Console.WriteLine("5 - Display the list of Vehicles in the Parking lot");
            Console.WriteLine("6 - Add the Vehicle on the Parking");
            Console.WriteLine("7 - Pick up a vehicle from Parking");
            Console.WriteLine("8 - Top up the balance of a specific Vehicle");
            choose = Console.ReadLine();
            if (!Regex.IsMatch(choose, "[1-8]{1}"))
            {
                Console.Clear();
                ActionRequest(menu);
            }

            switch (choose)
            {
                case "1":
                    Console.WriteLine(menu.DisplayBalance() + "\n");
                    break;
                case "2":
                    Console.WriteLine(menu.GetLastTransaction() + "\n");
                    break;
                case "3":
                    Console.Write("\nNumber of available / occupied parking spaces: ");
                    Console.WriteLine(menu.DisplayFreePlaces() + " / " + (menu.DisplayCapacity() - menu.DisplayFreePlaces()));
                    break;
                case "4":
                    Console.WriteLine(menu.GetTransactions() + "\n");
                    break;
                case "5":
                    Console.WriteLine(menu.DisplayVehicles());
                    break;
                case "6":
                    Console.WriteLine("\nAdding the Vehicle on the Parking");
                    Console.Write("Enter ID in format AA-0000-AA: ");
                    string id = Console.ReadLine();
                    if (!Regex.IsMatch(id, "[A-Z]{2}-[0-9]{4}-[A-Z]{2}"))
                    {
                        Console.WriteLine("Invalid ID Format! Press any key...");
                        Console.ReadKey();
                        Console.Clear();
                        ActionRequest(menu);
                    }

                    Console.WriteLine("Type: ");
                    Console.WriteLine("\t 1) Passenger Car");
                    Console.WriteLine("\t 2) Truck");
                    Console.WriteLine("\t 3) Bus");
                    Console.WriteLine("\t 4) Motorcycle");
                    Console.Write("Enter number of type a car: ");
                    string typeString = Console.ReadLine();
                    if (!Regex.IsMatch(typeString, "[1-4]{1}"))
                    {
                        Console.WriteLine("Invalid Type Format! Press any key...");
                        Console.ReadKey();
                        Console.Clear();
                        ActionRequest(menu);
                    }
                    int typeInt = Convert.ToInt32(typeString);
                    typeInt--;
                    Console.Write("Enter starting balance: ");
                    string balanceString = Console.ReadLine();
                    if (!Regex.IsMatch(balanceString, "[0-9]"))
                    {
                        if (Convert.ToDecimal(balanceString) <= 0)
                        Console.WriteLine("Invalid Balance! Press any key...");
                        Console.ReadKey();
                        Console.Clear();
                        ActionRequest(menu);
                    }
                    decimal balanceDecimal = Convert.ToDecimal(balanceString);
                    menu.AddVehicle(id, typeInt, balanceDecimal);
                    break;
                case "7":
                    Console.WriteLine("\nPick up a vehicle from Parking");
                    Console.Write("Enter vehicle number (id) in format AA-0000-AA: ");
                    id = Console.ReadLine();
                    if (!Regex.IsMatch(id, "[A-Z]{2}-[0-9]{4}-[A-Z]{2}"))
                    {
                        Console.WriteLine("Invalid ID Format! Press any key...");
                        Console.ReadKey();
                        Console.Clear();
                        ActionRequest(menu);
                    }
                    menu.DeleteVehicle(id);
                    break;
                case "8":
                    Console.WriteLine("\nTop up the balance of Vehicle");
                    Console.WriteLine("Enter vehicle number (id) in format AA-0000-AA: ");
                    id = Console.ReadLine();
                    if (!Regex.IsMatch(id, "[A-Z]{2}-[0-9]{4}-[A-Z]{2}"))
                    {
                        Console.WriteLine("Invalid ID Format! Press any key...");
                        Console.ReadKey();
                        Console.Clear();
                        ActionRequest(menu);
                    }
                    Console.Write("Enter recharge amount: ");
                    string sumString = Console.ReadLine();
                    decimal sumDecimal;
                    if (Decimal.TryParse(sumString, out sumDecimal))
                    {
                        menu.TopUpVehicle(id, sumDecimal);
                    }
                    else
                    {
                        Console.WriteLine("Invalid recharge amount Format! Press any key...");
                        Console.ReadKey();
                        Console.Clear();
                        ActionRequest(menu);
                    } 
                    break;
            }
            ActionRequest(menu);
        }
    }
}

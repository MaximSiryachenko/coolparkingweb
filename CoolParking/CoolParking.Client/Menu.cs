﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CoolParking.WebAPI;
using System.ComponentModel;
using CoolParking.BL.Models;
using CoolParking.BL.DTO;
using System.Runtime.ExceptionServices;
using System.Reflection.Metadata.Ecma335;
using System.Net;

namespace CoolParking.Client
{
    class Menu
    {
        private HttpClient _client;

        private string path = "https://localhost:5001/api";

        public string Path
        {
            get
            {
                return path;
            }
            set
            {
                path = value;
            }
        }

        public Menu()
        {
            _client = new HttpClient();
        }

        public decimal DisplayBalance()
        {
            Console.Write("\nCurrent Parking balance: ");
            var balance = _client.GetStringAsync($"{Path}/parking/balance").GetAwaiter().GetResult();
            return JsonConvert.DeserializeObject<decimal>(balance);
        }


        public int DisplayCapacity()
        {
            Console.Write("\nParking capacity: ");
            var capacity = _client.GetStringAsync($"{Path}/parking/capacity").GetAwaiter().GetResult();
            return JsonConvert.DeserializeObject<int>(capacity);
        }

        public int DisplayFreePlaces()
        {
            Console.Write("\nFree places on Parking: ");
            var freePlaces = _client.GetStringAsync($"{Path}/parking/freePlaces").GetAwaiter().GetResult();
            return JsonConvert.DeserializeObject<int>(freePlaces);
        }

        public string DisplayVehicles()
        {
            Console.Write("\nVehicles on Parking:\n");
            var vehicles = _client.GetStringAsync($"{Path}/vehicles").GetAwaiter().GetResult();
            var Veh =  JsonConvert.DeserializeObject<List<VehicleDTO>>(vehicles);
            string result = "";
            foreach(VehicleDTO v in Veh)
            {
                result += "id: " + Convert.ToString(v.Id) + " vehicleType: " + Convert.ToString(v.VehicleType) + " balance: " + Convert.ToString(v.Balance) + "\n";
            }
            return result;
        }

        public string DisplayVehicle(string id)
        {
            var vehicles = _client.GetStringAsync($"{Path}/vehicles/{id}").GetAwaiter().GetResult();
            var v = JsonConvert.DeserializeObject<VehicleDTO>(vehicles);
            return "id: " + Convert.ToString(v.Id) + " vehicleType: " + Convert.ToString(v.VehicleType) + " balance: " + Convert.ToString(v.Balance) + "\n"; ;
        }

        public void AddVehicle(string id, int vehicleType, decimal balance)
        {
            VehicleDTO v = new VehicleDTO(id, (VehicleType)vehicleType, balance);
            var myContent = JsonConvert.SerializeObject(v);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var result = _client.PostAsync($"{Path}/vehicles", byteContent).Result;
            Console.WriteLine((int)result.StatusCode + " - " + result.StatusCode);
            if ((int)result.StatusCode == 201)
                Console.WriteLine("Vehicle successful added!\n" + DisplayVehicle(id));
        }

        public async void DeleteVehicle(string id)
        {
            HttpResponseMessage response = await _client.DeleteAsync($"{Path}/vehicles/{id}");
            Console.WriteLine((int)response.StatusCode + " - " + response.StatusCode);
            if ((int)response.StatusCode == 200)
                Console.WriteLine($"Vehicle with Id: '{id}' successful delete! \n");
        }

        public string GetLastTransaction()
        {
            Console.WriteLine("\nLast transaction: ");
            var transaction = _client.GetStringAsync($"{Path}/transactions/last").GetAwaiter().GetResult();
            TransactionInfo t = JsonConvert.DeserializeObject<TransactionInfo>(transaction);
            return "vehicleId: " + t.Id + " sum: " + t.Sum + " transactionDate: " + t.Time;
        }

        public string GetTransactions()
        {
            Console.WriteLine("\nTransaction from Log: ");
            var transaction = _client.GetStringAsync($"{Path}/transactions/all").GetAwaiter().GetResult();
            return JsonConvert.DeserializeObject<string>(transaction);
        }

        public void TopUpVehicle(string id, decimal Sum)
        {
            VehicleDTO v = new VehicleDTO(id, Sum);
            var myContent = JsonConvert.SerializeObject(v);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var result = _client.PutAsync($"{Path}/transactions/topUpVehicle", byteContent).Result;
            Console.WriteLine((int)result.StatusCode + " - " + result.StatusCode);
            if ((int)result.StatusCode == 200)
                Console.WriteLine("Balance successfully replenished!\n" + DisplayVehicle(id));
        }
    }
}

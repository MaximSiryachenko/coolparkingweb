﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private ParkingService parkingService;
        public ParkingController(ParkingService service)
        {
            parkingService = service;
        }

        // GET api/parking/balance
        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetBalance()));
        }

        // GET api/parking/capacity
        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetCapacity()));
        }

        // GET api/parking/freePlaces
        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetFreePlaces()));
        }
    }
}
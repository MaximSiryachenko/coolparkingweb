﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using CoolParking.BL.DTO;
using Newtonsoft.Json;
using System.IO;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private ParkingService parkingService;
        public VehiclesController(ParkingService service)
        {
            parkingService = service;
        }

        // GET api/vehicles
        [HttpGet]
        public ActionResult<string> GetVehicles()
        {
            var str = JsonConvert.SerializeObject(parkingService.GetVehicles());
            return Ok(str);
        }

        // GET api/vehicles/id
        [HttpGet("{id}")]
        public ActionResult<string> GetVehicle(string id)
        {
            if (!parkingService.CheckNumber(id))
            {
                return BadRequest("BadRequest 400: Invalid format Id (AA-0000-AA)!");
            }
            foreach (Vehicle v in parkingService.GetVehicles())
            {
                if (v.Id == id)
                    return Ok(JsonConvert.SerializeObject(v));
            }
            return NotFound("NotFound 404: No vehicles found with this Id!");
        }

        // POST api/vehicles
        [HttpPost]
        public ActionResult<string> PostVehicle([FromBody]VehicleDTO vDto)
        {
            if (!parkingService.CheckNumber(vDto.Id) ||
                ((int)vDto.VehicleType < 0 || (int)vDto.VehicleType > 3) ||
                vDto.Balance < 0)
            {
                return BadRequest("BadRequest 400: Invalid format data!");
            }

            Vehicle v = new Vehicle(vDto.Id, vDto.VehicleType, vDto.Balance);
            if (parkingService.CheckVehicle(v))
            {
                return BadRequest("BadRequest 400: This Id already exist!");
            }

            parkingService.AddVehicle(v);
            return Created($"https://localhost:5001/api/vehicle/{vDto.Id}", vDto);
        }


        // DELETE api/vehicles/id
        [HttpDelete("{id}")]
        public ActionResult<string> DeleteVehicle(string id)
        {
            if (!parkingService.CheckNumber(id))
            {
                return BadRequest("BadRequest 400: Invalid format Id (AA-0000-AA)!");
            }
            foreach (Vehicle v in parkingService.GetVehicles())
            {
                if (v.Id == id)
                {
                    parkingService.RemoveVehicle(id);
                    return NoContent();
                }
            }
            return NotFound();
        }
    }
}
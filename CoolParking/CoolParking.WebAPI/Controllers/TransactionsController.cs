﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;
using Newtonsoft.Json;
using System.Transactions;
using CoolParking.BL.Models;
using System.IO;
using CoolParking.BL.DTO;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private ParkingService parkingService;
        public TransactionsController(ParkingService service)
        {
            parkingService = service;
        }

        // GET api/transactions/last
        [HttpGet("last")]
        public ActionResult<string> GetLastTransaction()
        {
            TransactionInfo[] t = parkingService.GetLastParkingTransactions();
            if (t.Length > 0)
            {
                var str = JsonConvert.SerializeObject(t[^1], Formatting.Indented);
                return Ok(str);
            }
            return NoContent();
        }

        // GET api/transactions/all
        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            string path =@".\Transactions.log";
            if (!System.IO.File.Exists(path))
            {
                return NotFound("NotFound 404: Transactions.log not exist!");
            }
            return Ok(JsonConvert.SerializeObject(parkingService.ReadFromLog()));
        }

        // PUT api/transactions/topUpVehicle
        [HttpPut("topUpVehicle")]
        public ActionResult<string> PutTopUpVehicle([FromBody]VehicleDTO vDto)
        {
            if (!parkingService.CheckNumber(vDto.id) || vDto.Sum <= 0)
            {
                return BadRequest("BadRequest 400: Invalid format data!");
            }
            foreach (Vehicle v in parkingService.GetVehicles())
            {
                if (v.Id == vDto.id)
                {
                    parkingService.TopUpVehicle(vDto.id, vDto.Sum);
                    vDto.Balance = v.Balance;
                    return Ok(vDto);
                }
            }
            return NotFound();
        }
    }
}